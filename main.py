import os
import random
from functions import *

player_pieces_list = []
player_moves_list = []
computer_pieces_list = []
computer_moves_list = []
p = Checkers("X", "Ẍ", "O", "Ö", player_pieces_list, player_moves_list, 1)
c = Checkers("O", "Ö", "X", "Ẍ", computer_pieces_list, computer_moves_list, (-1))

printing_board()
while True:
    p.avaliable_piece()
    if not p.avaliable_pieces_list and not p.capturing_moves:
        print("Computer won")
        break
    if not p.capturing_moves:
        print(p.avaliable_pieces_list)
        player_move_up = input("Avaliable pieces: ").upper()
        while player_move_up not in p.avaliable_pieces_list:
            player_move_up = input("Avaliable pieces: ").upper()
        print(p.possible_places(player_move_up))
        player_move_down = input("Avaliable fields: ").upper()
        while player_move_down not in p.piece_places_list:
            player_move_down = input("Avaliable fields: ").upper()
        p.change_position(player_move_up, player_move_down)
        os.system('cls')
    else:
        p.player_multi_capturing()
        os.system('cls')

    printing_board()
    input("Press enter...")
    os.system('cls')
    c.avaliable_piece()
    if not c.avaliable_pieces_list and not c.capturing_moves:
        print("Player won")
        break
    if not c.capturing_moves:
        computer_piece = random.choice(c.avaliable_pieces_list)
        c.possible_places(computer_piece)
        computer_plac = random.choice(c.piece_places_list)
        c.change_position(computer_piece, computer_plac)
        printing_board()
        print(f"Computer is moving from {computer_piece} to {computer_plac}")
    else:
        c.computer_multi_capturing()


