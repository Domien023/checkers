import os
import random

alph_row = [' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
board = [
    [' ', 'O', ' ', 'O', ' ', 'O', ' ', 'O'],
    ['O', ' ', 'O', ' ', 'O', ' ', 'O', ' '],
    [' ', 'O', ' ', 'O' ,' ', 'O' ,' ', 'O'],
    ['-', ' ', '-', ' ', '-', ' ', '-', ' '],
    [' ', '-' ,' ', '-', ' ', '-', ' ', '-'],
    ['X', ' ', 'X', ' ', 'X', ' ', 'X', ' '],
    [' ', 'X', ' ', 'X', ' ', 'X', ' ', 'X'],
    ['X', ' ', 'X', ' ', 'X', ' ', 'X', ' ']
]

class Checkers:
    def __init__(self, piece, queen,
                 piece_opponent, queen_opponent, avaliable_pieces_list,
                 piece_places_list, row_change):
        self.piece = piece
        self.queen = queen
        self.piece_opponent = piece_opponent
        self.queen_opponent = queen_opponent
        self.avaliable_pieces_list = avaliable_pieces_list
        self.piece_places_list = piece_places_list
        self.row_change = row_change
        self.capturing_moves = []
        self.column = {
            'A': '0',
            'B': '1',
            'C': '2',
            'D': '3',
            'E': '4',
            'F': '5',
            'G': '6',
            'H': '7',
            0: 'A',
            1: 'B',
            2: 'C',
            3: 'D',
            4: 'E',
            5: 'F',
            6: 'G',
            7: 'H'
        }


    def avaliable_piece(self):
        self.avaliable_pieces_list.clear()
        self.capturing_moves.clear()
        row_index = 0
        for row in board:
            column_index = 0
            for pieces in row:
                if pieces == self.queen:
                    self.queen_capturing(row_index, column_index)
                    self.avaliable_queen(row_index, column_index)
                if pieces == self.piece:
                    try:
                        if row_index!=0 or self.row_change==-1:
                            self.piece_capturing(row_index, column_index)
                            if board[row_index - self.row_change][column_index - 1]=='-' and \
                                    column_index!=0 or \
                                    board[row_index - self.row_change][column_index + 1]=='-':
                                self.avaliable_pieces_list.append(f'{self.column[column_index]}{row_index}')
                    except:
                        pass
                column_index += 1
            row_index += 1

    def avaliable_queen(self, row_num, column_num):
        if column_num!=0 and row_num!=0 and board[row_num - 1][column_num - 1]=='-':
            self.avaliable_pieces_list.append(f"{self.column[column_num]}{row_num}")
        elif column_num!=7 and row_num!=0 and board[row_num - 1][column_num + 1]=='-':
            self.avaliable_pieces_list.append(f"{self.column[column_num]}{row_num}")
        elif column_num!=0 and row_num!=7 and board[row_num + 1][column_num - 1]=='-':
            self.avaliable_pieces_list.append(f"{self.column[column_num]}{row_num}")
        elif column_num!=7 and row_num!=7 and board[row_num+1][column_num+1]=='-':
            self.avaliable_pieces_list.append(f"{self.column[column_num]}{row_num}")

    def piece_capturing(self, row_num, colum_num):
        row_num = int(row_num)
        colum_num = int(colum_num)
        if board[row_num - 1][colum_num - 1]==self.piece_opponent or\
                board[row_num - 1][colum_num - 1]==self.queen_opponent:
            if colum_num!=1 and colum_num!=0 and row_num!=0 and row_num!=1:
                if board[row_num - 2][colum_num - 2] == "-":
                    self.capturing_moves.append(f"{self.column[colum_num]}{row_num}!")
                    self.piece_places_list.append(f"{self.column[colum_num - 2]}{row_num - 2}")
        if colum_num!=1 and colum_num!=0 and row_num!=6 and row_num!=7:
            if board[row_num + 1][colum_num - 1]==self.piece_opponent or\
                    board[row_num + 1][colum_num - 1]==self.queen_opponent:
                if board[row_num + 2][colum_num - 2] == "-":
                    self.capturing_moves.append(f"{self.column[colum_num]}{row_num}!")
                    self.piece_places_list.append(f"{self.column[colum_num - 2]}{row_num + 2}")
        try:
            if board[row_num - 1][colum_num + 1]==self.piece_opponent or\
                    board[row_num - 1][colum_num + 1]==self.queen_opponent:
                if row_num!=0 and row_num!=1:
                    if board[row_num - 2][colum_num + 2] == "-":
                        self.capturing_moves.append(f"{self.column[colum_num]}{row_num}!")
                        self.piece_places_list.append(f"{self.column[colum_num + 2]}{row_num - 2}")
            if board[row_num + 1][colum_num + 1]==self.piece_opponent or \
                    board[row_num + 1][colum_num + 1]==self.queen_opponent:
                if board[row_num + 2][colum_num + 2] == "-":
                    self.capturing_moves.append(f"{self.column[colum_num]}{row_num}!")
                    self.piece_places_list.append(f"{self.column[colum_num + 2]}{row_num + 2}")
        except:
            pass

    def queen_capturing(self, row_num, column_num):
        row_num = int(row_num)
        column_num = int(column_num)
        for change_index in range(1, 8):
            if (row_num - change_index)>0 and (column_num - change_index)>0:
                position = board[row_num - change_index][column_num - change_index]
                if position==self.queen_opponent or position==self.piece_opponent:
                    if board[row_num - change_index - 1][column_num - change_index - 1] == '-':
                        self.capturing_moves.append(f"{self.column[column_num]}{row_num}!")
                        if self.piece_places_list != None:
                            self.piece_places_list.append(f"{self.column[column_num - change_index - 1]}{row_num - change_index - 1}")
                        break
                    else:
                        break
                elif position=="X" or position=="O" or position=="Ẍ" or position=="Ö":
                    break
        for change_index in range(1, 8):
            if (row_num + change_index)<7 and (column_num - change_index)>0:
                position = board[row_num + change_index][column_num - change_index]
                if position==self.queen_opponent or position==self.piece_opponent:
                    if board[row_num + change_index + 1][column_num - change_index - 1] == '-':
                        self.capturing_moves.append(f"{self.column[column_num]}{row_num}!")
                        if self.piece_places_list != None:
                            self.piece_places_list.append(f"{self.column[column_num - change_index - 1]}{row_num + change_index + 1}")
                        break
                    else:
                        break
                elif position=="X" or position=="O" or position=="Ẍ" or position=="Ö":
                    break
        for change_index in range(1, 8):
            if (row_num - change_index)>0 and (column_num + change_index)<7:
                position = board[row_num - change_index][column_num + change_index]
                if position==self.queen_opponent or position==self.piece_opponent:
                    if board[row_num - change_index - 1][column_num + change_index + 1] == '-':
                        self.capturing_moves.append(f"{self.column[column_num]}{row_num}!")
                        if self.piece_places_list != None:
                            self.piece_places_list.append(f"{self.column[column_num + change_index + 1]}{row_num - change_index - 1}")
                        break
                    else:
                        break
                elif position=="X" or position=="O" or position=="Ẍ" or position=="Ö":
                    break
        for change_index in range(1, 8):
            if (row_num + change_index)<7 and (column_num + change_index)<7:
                position = board[row_num + change_index][column_num + change_index]
                if position==self.queen_opponent or position==self.piece_opponent:
                    if board[row_num + change_index + 1][column_num + change_index + 1] == '-':
                        self.capturing_moves.append(f"{self.column[column_num]}{row_num}!")
                        if self.piece_places_list != None:
                            self.piece_places_list.append(f"{self.column[column_num + change_index + 1]}{row_num + change_index + 1}")
                        break
                    else:
                        break
                elif position=="X" or position=="O" or position=="Ẍ" or position=="Ö":
                    break

                pass

    def possible_places(self, piece):
        self.piece_places_list.clear()
        colum_num = piece[0]
        row_num = piece[1]
        row_num = int(row_num)
        colum_num = self.column[colum_num]
        colum_num = int(colum_num)
        if board[row_num][colum_num]=="Ẍ" or board[row_num][colum_num]=="Ö":
            self.queen_capturing(row_num, colum_num)
        else:
            self.piece_capturing(row_num, colum_num)
        if not self.piece_places_list:
            if board[row_num][colum_num]=="Ẍ" or board[row_num][colum_num]=="Ö":
                self.queen_places(row_num, colum_num)
            elif colum_num == 0:
                if board[row_num - self.row_change][colum_num + 1] == "-":
                    self.piece_places_list.append(f"{self.column[colum_num + 1]}{row_num - self.row_change}")
            else:
                value_index = colum_num - 1
                for index in board[row_num - self.row_change][colum_num - 1 : colum_num + 2]:
                    if index == "-":
                        self.piece_places_list.append(f"{self.column[value_index]}{row_num - self.row_change}")
                    value_index += 1
        return self.piece_places_list

    def queen_places(self, row_num, column_num):
        for change_index in range(1, 8):
            if (column_num-change_index)>-1 and (row_num-change_index)>-1 and \
                    board[row_num - change_index][column_num - change_index]=='-':
                self.piece_places_list.append(f"{self.column[column_num - change_index]}{row_num - change_index}")
            else:
                break
        for change_index in range(1, 8):
            if (column_num+change_index)<8 and (row_num-change_index)>-1 and \
                    board[row_num - change_index][column_num + change_index]=='-':
                self.piece_places_list.append(f"{self.column[column_num + change_index]}{row_num - change_index}")
            else:
                break
        for change_index in range(1, 8):
            if (column_num-change_index)>-1 and (row_num+change_index)<8 and \
                    board[row_num + change_index][column_num - change_index]=='-':
                self.piece_places_list.append(f"{self.column[column_num - change_index]}{row_num + change_index}")
            else:
                break
        for change_index in range(1, 8):
            if (column_num+change_index)<8 and (row_num+change_index)<8 and \
                    board[row_num + change_index][column_num + change_index]=='-':
                self.piece_places_list.append(f"{self.column[column_num + change_index]}{row_num + change_index}")
            else:
                break

    def player_multi_capturing(self):
        print(self.capturing_moves)
        player_move_up = input("Avaliable pieces: ").upper()
        while player_move_up not in self.avaliable_pieces_list:
            if f"{player_move_up}!" in self.capturing_moves:
                break
            player_move_up = input("Avaliable pieces: ").upper()
        print(self.possible_places(player_move_up))
        player_move_down = input("Avaliable fields: ").upper()
        while player_move_down not in self.piece_places_list:
            player_move_down = input("Avaliable fields: ").upper()
        self.change_position(player_move_up, player_move_down)
        while True:
            os.system('cls')
            printing_board()
            self.avaliable_piece()
            if f"{player_move_down}!" in self.capturing_moves:
                print(f"You must make another capture with {player_move_down} piece!")
                player_move_up = player_move_down
                print(self.possible_places(player_move_up))
                player_move_down = input("Avaliable fields: ").upper()
                self.change_position(player_move_up, player_move_down)
            else:
                break

    def computer_multi_capturing(self):
        computer_piece = random.choice(self.capturing_moves)
        self.possible_places(computer_piece)
        computer_plac = random.choice(self.piece_places_list)
        self.change_position(computer_piece, computer_plac)
        while True:
            os.system('cls')
            printing_board()
            print(f"Computer is moving from {computer_piece} to {computer_plac}")
            self.avaliable_piece()
            if f"{computer_plac}!" in self.capturing_moves:
                computer_piece = computer_plac
                print(self.possible_places(computer_piece))
                computer_plac = random.choice(self.piece_places_list)
                self.change_position(computer_piece, computer_plac)
                input("Press enter...")
            else:
                break

    def change_position(self, place_from, place_to):
        place_from_inx_1 = place_from[1]  # rows
        place_from_inx_2 = place_from[0]  # columns
        place_from_inx_2 = self.column[place_from_inx_2]
        place_from_inx_1 = int(place_from_inx_1)
        place_from_inx_2 = int(place_from_inx_2)
        place_to_inx_1 = place_to[1]  # rows
        place_to_inx_2 = place_to[0]  # columns
        place_to_inx_2 = self.column[place_to_inx_2]
        place_to_inx_1 = int(place_to_inx_1)
        place_to_inx_2 = int(place_to_inx_2)
        if (place_to_inx_2 - place_from_inx_2)<0 and (place_to_inx_1 - place_from_inx_1)<0:
            board[place_to_inx_1 + 1][place_to_inx_2 + 1] = "-"
        elif (place_to_inx_2 - place_from_inx_2)>0 and (place_to_inx_1 - place_from_inx_1)<0:
            board[place_to_inx_1 + 1][place_to_inx_2 - 1] = "-"
        elif (place_to_inx_2 - place_from_inx_2)<0 and (place_to_inx_1 - place_from_inx_1)>0:
            board[place_to_inx_1 - 1][place_to_inx_2 + 1] = "-"
        elif (place_to_inx_2 - place_from_inx_2)>0 and (place_to_inx_1 - place_from_inx_1)>0:
            board[place_to_inx_1 - 1][place_to_inx_2 - 1] = "-"
        if place_to_inx_1==0 and self.piece=="X":
            board[place_to_inx_1][place_to_inx_2] = "Ẍ"
        elif place_to_inx_1==7 and self.piece=="O":
            board[place_to_inx_1][place_to_inx_2] = "Ö"
        elif board[place_from_inx_1][place_from_inx_2] == self.queen:
            board[place_to_inx_1][place_to_inx_2] = self.queen
        else:
            board[place_to_inx_1][place_to_inx_2] = self.piece

        board[place_from_inx_1][place_from_inx_2] = "-"


def printing_board():
    for str in alph_row:
        print(str, end=" ")
    print("")
    num = 0
    for row in board:
        print(num, end=" ")
        for column in row:
            print(column, end=" ")
        print('')
        num += 1
